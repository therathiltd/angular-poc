# Pitch

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.

## Development server

Run `git clone {repo-link}` to clone the codebase.

## Development server

Run `npm i` to install all package and dependencies.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


Supported Features : 
1. Saving any number of links with a project 
2. Saving projects in local Storage , so data persists even after refresh
3. Component Wise Structure and readysupport for future integrations of login etc.
4. Responsive, Mobile Friendly Design.

Unsupported Features : 
1. Google OAuth - I tried it but it requires a valid domain, doesnt work on localhost. So, I could not add here.

Possible Enhancements:
1. Be able to add/remove links to existing projects
2. Be able to delete projects.