import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data/data.service';
@Component({
  selector: 'app-projectdetail',
  templateUrl: './project.detail.component.html',
  styleUrls: ['./project.detail.component.scss']
})
/**
 * This component displays the links in a particular project.
 * Its is the most detailed view of a project in our app.
 */
export class ProjectDetailComponent {
    project:any;
    constructor(
        private route: ActivatedRoute,
        private dataService : DataService
      ) {}

    ngOnInit()
    {
        const id = +this.route.snapshot.paramMap.get('id');
        this.project = this.dataService.getProject(id);

    }
}
