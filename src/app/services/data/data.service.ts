import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

interface data{
  name:string;
  links:[]
}

@Injectable({
  providedIn: 'root'
})
/**
 * This service is responsible for storing the projects and links.
 * It provides relevant functions to add projects, retrieve their details.
 * All data is stored in local storgae.
 */
export class DataService {

   projects = new BehaviorSubject([]); // Subscribe to this if you want project updates.

  constructor() { 

    this.saveProjects(this.getProjects()); // Trigger Initial Subscription
  }
  getProjects()
  {
    let projects = JSON.parse(localStorage.getItem('projects'));
    if (projects == null) {
      projects = [];
    }
    console.log(projects);
    return projects;
  }
  addProject(name,links)
  {
    let projects = this.getProjects();
    let id = projects.length + 1;
    let project = Object();
    project.id=id;
    project.name=name;
    project.links= links;
    projects.push(project);
    this.saveProjects(projects);
    return id;

  }
  saveProjects(projects)
  {

    localStorage.setItem('projects', JSON.stringify(projects));
    this.projects.next(projects)

  }
  getProject(projectId)
  {
    let projects = this.getProjects();
    var project = projects.find(function(element) { 
      return element.id ==projectId; 
    }); 
    if(project===undefined)
    {
      return [];
    }
    console.log("Returning",project);
    return project;
  }

  addLink(link,projectId)
  {
    let projects = this.getProjects();
    let project =  projects.find(function(element) { 
      return element.id =projectId; 
    }); 
    project.links.push(link);
    this.saveProjects(projects);

  }
  

}
