import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-newprojectdialog',
    templateUrl: './newprojectdialog.component.html',
})
export class NewProjectDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<NewProjectDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { console.log(data); }

    onNoClick(): void {
        this.dialogRef.close();
    }
    add_link() {
        this.data.links.push("");
    }
    remove_link(link) {
        this.data.links.splice([this.data.links.indexOf(link)], 1);
        // let row = this.data.links.de(element=>element=link);

    }
    getIndex(index: number, obj: any): any {
        return index;
    }
    formisvalid() {
        if (this.data.projectname===undefined || this.data.projectname.length == 0) {
            return false;

        }

        for (let link of this.data.links) {
            if (link.length == 0) {
                return false;
            }
        }
        return true;

    }

}
