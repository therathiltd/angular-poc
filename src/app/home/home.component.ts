import { Component } from '@angular/core';

import { MatDialog } from '@angular/material';
import { NewProjectDialogComponent } from '../shared/newprojectdialog/newprojectdialog.component';
import { DataService } from '../services/data/data.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {
    links: any;
    projectname: string;
    projects: any;

    constructor(public dialog: MatDialog, private dataService: DataService , private router: Router) {

        this.links = [
            "https://stackoverflow.com",
            "https://www.reddit.com/r/dataisbeautiful",

        ]
    }
    ngOnInit() {
        this.projects = this.dataService.getProjects();
        console.log("Got projects");
        this.dataService.projects.subscribe( (projects)=>
        {
            this.projects = projects;
        }
  );

}
openDialog(): void {
    const dialogRef = this.dialog.open(NewProjectDialogComponent, {
        width: '500px',
        data: { projectname: this.projectname, links: this.links }
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);
        //this.projectname = result.projectname;
        //this.links = result.links;
        let projectId = this.dataService.addProject(result.projectname, result.links);
        this.router.navigate(['/project/',projectId])
    });
}
}
