import { Component } from '@angular/core';

import { DataService } from '../services/data/data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
/**
 * This Component is for te list view of projects. It simply displays list of projects with link to the project.
 */
export class ProjectsComponent {
    projects: any;

    constructor(private dataService: DataService , private router: Router) {}
    ngOnInit() {
        this.projects = this.dataService.getProjects();
        console.log("Got projects");
        this.dataService.projects.subscribe( (projects)=>
        {
            this.projects = projects;
        }
  );

}
}
