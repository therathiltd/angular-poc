import { Routes } from '@angular/router';

import { ProjectsComponent } from '../projects/projects.component';
import { ProjectDetailComponent } from '../projectDetail/project.detail.component';
import { HomeComponent} from '../home/home.component';
export const MaterialRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'projects',
    component: ProjectsComponent
  },
  {
    path: 'project/:id',
    component: ProjectDetailComponent
  },
];
